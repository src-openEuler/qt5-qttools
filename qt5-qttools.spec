# Disable automatic .la file removal
%global __brp_remove_la_files %nil

%global qt_module qttools

Name:          qt5-qttools
Version:       5.15.10
Release:       4
Summary:       Qt5 QtTool module
License:       LGPL-3.0-only OR GPL-3.0-only WITH Qt-GPL-exception-1.0
Url:           http://www.qt.io
%global majmin %(echo %{version} | cut -d. -f1-2)
Source0:       https://download.qt.io/official_releases/qt/%{majmin}/%{version}/submodules/%{qt_module}-everywhere-opensource-src-%{version}.tar.xz
Source1:       assistant.desktop
Source2:       designer.desktop
Source3:       linguist.desktop
Source4:       qdbusviewer.desktop

Patch0:        qttools-opensource-src-5.13.2-runqttools-with-qt5-suffix.patch
Patch1:        qttools-opensource-src-5.7-add-libatomic.patch
Patch2:        0001-Link-against-libclang-cpp.so-instead-of-the-clang-co.patch
Patch3:        0001-modify-lupdate-qt5-run-error.patch
Patch4:        0001-update-Qt5LinguistToolsMacros.cmake.patch

BuildRequires: make
BuildRequires: cmake desktop-file-utils /usr/bin/file qt5-rpm-macros >= %{version}
BuildRequires: qt5-qtbase-private-devel qt5-qtbase-devel >= %{version} qt5-qtbase-static >= %{version}
BuildRequires: clang-devel llvm-devel qt5-qtdeclarative-devel >= %{version} pkgconfig(Qt5Qml) qt5-qtdeclarative-static >= %{version}
%{?_qt5:Requires: %{_qt5} = %{_qt5_version}}

Requires:      %{name}-common = %{version}-%{release}
%description
%{summary}.

%package common
Summary: Common files for %{name}
BuildArch:     noarch
Obsoletes:     qt5-qttools-libs-clucene < 5.9.0
%if ! 0%{?webkit}
Obsoletes:     qt5-designer-plugin-webkit < 5.9.0
%endif
%description common
%{summary}.

%package devel
Summary:       %{name} development files
Requires:      %{name} = %{version}-%{release} %{name}-libs-designer = %{version}-%{release}
Requires:      %{name}-libs-designercomponents = %{version}-%{release} %{name}-libs-help = %{version}-%{release}
Requires:      qt5-doctools = %{version}-%{release} qt5-designer = %{version}-%{release}
Requires:      qt5-linguist = %{version}-%{release} qt5-qtbase-devel
%description devel
%{summary}.

%package static
Summary:      Static library files for %{name}
Requires:     %{name}-devel%{?_isa} = %{version}-%{release}
%description static
%{summary}.

%package libs-designer
Summary:      Qt5 Designer runtime library
Requires:     %{name}-common = %{version}-%{release}
%description libs-designer
%{summary}.

%package libs-designercomponents
Summary:      Qt5 Designer Components runtime library
Requires:     %{name}-common = %{version}-%{release}
%description libs-designercomponents
%{summary}.

%package libs-help
Summary:      Qt5 Help runtime library
Requires:     %{name}-common = %{version}-%{release}
# when split happened
Conflicts:    qt5-tools < 5.4.0-0.2
%description libs-help
%{summary}.

%package -n qt5-assistant
Summary:       Documentation browser for Qt5
Requires:     %{name}-common = %{version}-%{release}
%description -n qt5-assistant
%{summary}.

%package -n qt5-designer
Summary:       Design GUIs for Qt5 applications
Requires:      %{name}-libs-designer%{?_isa} = %{version}-%{release}
Requires:      %{name}-libs-designercomponents%{?_isa} = %{version}-%{release}
%description -n qt5-designer
%{summary}.

%if 0%{?webkit}
%package -n qt5-designer-plugin-webkit
Summary:       Qt5 designer plugin for WebKit
BuildRequires: pkgconfig(Qt5WebKitWidgets)
Requires:      %{name}-libs-designer%{?_isa} = %{version}-%{release}
%description -n qt5-designer-plugin-webkit
%{summary}.
%endif

%package -n qt5-linguist
Summary:      Qt5 Linguist Tools
Requires:     %{name}-common = %{version}-%{release}
%description -n qt5-linguist
Tools to add translations to Qt5 applications.

%package -n qt5-qdbusviewer
Summary:      D-Bus debugger and viewer
Requires:     %{name}-common = %{version}-%{release}
%{?_qt5:Requires: %{_qt5}%{?_isa} >= %{_qt5_version}}
%description -n qt5-qdbusviewer
QDbusviewer can be used to inspect D-Bus objects of running programs
and invoke methods on those objects.

%package -n qt5-doctools
Summary: Qt5 doc tools package
Provides: qt5-qdoc = %{version}
Obsoletes: qt5-qdoc < 5.8.0
Provides: qt5-qhelpgenerator = %{version}
Obsoletes: qt5-qhelpgenerator < 5.8.0
Provides: qt5-qtattributionsscanner = %{version}
Obsoletes: qt5-qtattributionsscanner < 5.8.0
Requires: qt5-qtattributionsscanner = %{version}

%description -n qt5-doctools
%{summary}.

%package examples
Summary: Programming examples for %{name}
Requires: %{name}-common = %{version}-%{release}
%description examples
%{summary}.


%prep
%setup -q -n %{qt_module}-everywhere-src-%{version}

%patch -P0 -p1 -b ..runqttools-with-qt5-suffix.patch
%ifarch %{mips32}
%patch -P1 -p1 -b .libatomic
%endif
%patch -P2 -p1 -b .libclang-cpp
%patch -P3 -p1
%patch -P4 -p1

%build
%{qmake_qt5} \
  %{?no_examples}

%make_build


%install
%make_install INSTALL_ROOT=%{buildroot}
desktop-file-install --dir=%{buildroot}%{_datadir}/applications --vendor="qt5" \
  %{SOURCE1} %{SOURCE2} %{SOURCE3} %{SOURCE4}

install -Dp -m 644 src/assistant/assistant/images/assistant.png %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/assistant-qt5.png
install -Dp -m 644 src/assistant/assistant/images/assistant-128.png %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/assistant-qt5.png
install -Dp -m 644 src/designer/src/designer/images/designer.png %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/designer-qt5.png
install -Dp -m 644 src/qdbus/qdbusviewer/images/qdbusviewer.png %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/qdbusviewer-qt5.png
install -Dp -m 644 src/qdbus/qdbusviewer/images/qdbusviewer-128.png %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/qdbusviewer-qt5.png

for icon in src/linguist/linguist/images/icons/linguist-*-32.png ; do
  size=$(echo $(basename ${icon}) | cut -d- -f2)
  install -p -m 644 -D ${icon} %{buildroot}%{_datadir}/icons/hicolor/${size}x${size}/apps/linguist-qt5.png
done

mkdir %{buildroot}%{_bindir}
cd %{buildroot}%{_qt5_bindir}
for x in * ; do
  case "${x}" in
   assistant|designer|lconvert|linguist|lrelease|lupdate|lprodump|pixeltool|qcollectiongenerator|qdbus \
   |qdbusviewer|qhelpconverter|qhelpgenerator|qtplugininfo|qtattributionsscanner)
      ln -v  ${x} %{buildroot}%{_bindir}/${x}-qt5
      ln -sv ${x} ${x}-qt5
      ;;
    *)
      ln -v  ${x} %{buildroot}%{_bindir}/${x}
      ;;
  esac
done
cd -

cd  %{buildroot}%{_qt5_libdir}
for prl_file in libQt5*.prl ; do
  sed -i -e "/^QMAKE_PRL_BUILD_DIR/d" ${prl_file}
  if [ -f "$(basename ${prl_file} .prl).so" ]; then
    rm -fv "$(basename ${prl_file} .prl).la"
    sed -i -e "/^QMAKE_PRL_LIBS/d" ${prl_file}
  fi
done
cd -

sed -i -e 's| Qt5UiPlugin||g' %{buildroot}%{_qt5_libdir}/pkgconfig/Qt5Designer.pc


%check
export PKG_CONFIG_PATH=%{buildroot}%{_libdir}/pkgconfig:$PKG_CONFIG_PATH
pkg-config --print-requires --print-requires-private Qt5Designer
export CMAKE_PREFIX_PATH=%{buildroot}%{_qt5_prefix}:%{buildroot}%{_prefix}
export PATH=%{buildroot}%{_qt5_bindir}:%{_qt5_bindir}:$PATH
export LD_LIBRARY_PATH=%{buildroot}%{_qt5_libdir}
mkdir tests/auto/cmake/%{_target_platform}
cd tests/auto/cmake/%{_target_platform}
cmake ..
ctest --output-on-failure ||:
cd -

cd  %{buildroot}%{_datadir}/icons
for RES in $(ls hicolor); do
  for APP in designer assistant linguist qdbusviewer; do
    if [ -e hicolor/$RES/apps/${APP}*.* ]; then
      file hicolor/$RES/apps/${APP}*.* | grep "$(echo $RES | sed 's/x/ x /')"
    fi
  done
done
cd -

%files
%{_bindir}/qdbus-qt5
%{_bindir}/qtpaths
%{_qt5_bindir}/qdbus
%{_qt5_bindir}/qdbus-qt5
%{_qt5_bindir}/qtpaths

%files common
%license LICENSE.LGPL*

%files  libs-designer
%{_qt5_libdir}/libQt5Designer.so.5*
%dir %{_qt5_libdir}/cmake/Qt5Designer/

%files  libs-designercomponents
%{_qt5_libdir}/libQt5DesignerComponents.so.5*

%files  libs-help
%{_qt5_libdir}/libQt5Help.so.5*

%files -n qt5-assistant
%{_bindir}/assistant-qt5
%{_qt5_bindir}/assistant*
%{_datadir}/applications/*assistant.desktop
%{_datadir}/icons/hicolor/*/apps/assistant*.*

%files -n qt5-doctools
%{_bindir}/qdoc*
%{_qt5_bindir}/qdoc*
%{_bindir}/qdistancefieldgenerator*
%{_bindir}/qhelpgenerator*
%{_qt5_bindir}/qdistancefieldgenerator*
%{_qt5_bindir}/qhelpgenerator*
%{_bindir}/qtattributionsscanner-qt5
%{_qt5_bindir}/qtattributionsscanner*

%files -n qt5-designer
%{_bindir}/designer*
%{_qt5_bindir}/designer*
%{_datadir}/applications/*designer.desktop
%{_datadir}/icons/hicolor/*/apps/designer*.*
%{_qt5_libdir}/cmake/Qt5DesignerComponents/Qt5DesignerComponentsConfig*.cmake

%if 0%{?webkit}
%files -n qt5-designer-plugin-webkit
%{_qt5_plugindir}/designer/libqwebview.so
%{_qt5_libdir}/cmake/Qt5Designer/Qt5Designer_QWebViewPlugin.cmake
%endif

%files -n qt5-linguist
%{_bindir}/linguist*
%{_qt5_bindir}/linguist*
# phrasebooks used by linguist
%{_qt5_datadir}/phrasebooks/
%{_datadir}/applications/*linguist.desktop
%{_datadir}/icons/hicolor/*/apps/linguist*.*
# linguist friends
%{_bindir}/lconvert*
%{_bindir}/lrelease*
%{_bindir}/lupdate*
%{_bindir}/lprodump*
%{_qt5_bindir}/lconvert*
%{_qt5_bindir}/lrelease*
%{_qt5_bindir}/lupdate*
%{_qt5_bindir}/lprodump*
# cmake config
%dir %{_qt5_libdir}/cmake/Qt5LinguistTools/
%{_qt5_libdir}/cmake/Qt5LinguistTools/Qt5LinguistToolsConfig*.cmake
%{_qt5_libdir}/cmake/Qt5LinguistTools/Qt5LinguistToolsMacros.cmake

%files -n qt5-qdbusviewer
%{_bindir}/qdbusviewer*
%{_qt5_bindir}/qdbusviewer*
%{_datadir}/applications/*qdbusviewer.desktop
%{_datadir}/icons/hicolor/*/apps/qdbusviewer*.*

%files devel
%{_bindir}/pixeltool*
%{_bindir}/qcollectiongenerator*
#{_bindir}/qhelpconverter*
%{_bindir}/qtdiag*
%{_bindir}/qtplugininfo*
%{_qt5_bindir}/pixeltool*
%{_qt5_bindir}/qtdiag*
%{_qt5_bindir}/qcollectiongenerator*
#{_qt5_bindir}/qhelpconverter*
%{_qt5_bindir}/qtplugininfo*
%{_qt5_headerdir}/QtDesigner/
%{_qt5_headerdir}/QtDesignerComponents/
%{_qt5_headerdir}/QtHelp/
%{_qt5_headerdir}/QtUiPlugin
%{_qt5_libdir}/libQt5Designer*.prl
%{_qt5_libdir}/libQt5Designer*.so
%{_qt5_libdir}/libQt5Help.prl
%{_qt5_libdir}/libQt5Help.so
%{_qt5_libdir}/Qt5UiPlugin.la
%{_qt5_libdir}/libQt5UiPlugin.prl
%{_qt5_libdir}/cmake/Qt5Designer/Qt5DesignerConfig*.cmake
%dir %{_qt5_libdir}/cmake/Qt5Help/
%{_qt5_libdir}/cmake/Qt5Help/Qt5HelpConfig*.cmake
%{_qt5_libdir}/cmake/Qt5UiPlugin/
%{_qt5_libdir}/pkgconfig/Qt5Designer.pc
%{_qt5_libdir}/pkgconfig/Qt5Help.pc
%{_qt5_libdir}/pkgconfig/Qt5UiPlugin.pc
%{_qt5_archdatadir}/mkspecs/modules/qt_lib_designer.pri
%{_qt5_archdatadir}/mkspecs/modules/qt_lib_designer_private.pri
%{_qt5_archdatadir}/mkspecs/modules/qt_lib_designercomponents_private.pri
%{_qt5_archdatadir}/mkspecs/modules/qt_lib_help.pri
%{_qt5_archdatadir}/mkspecs/modules/qt_lib_help_private.pri
%{_qt5_archdatadir}/mkspecs/modules/qt_lib_uiplugin.pri
# putting these here for now, new stuff in 5.14, review for accuracy -- rdieter
%{_qt5_libdir}/cmake/Qt5AttributionsScannerTools/
%{_qt5_libdir}/cmake/Qt5DocTools/

%files static
%{_qt5_headerdir}/QtUiTools/
%{_qt5_libdir}/libQt5UiTools.*a
%{_qt5_libdir}/libQt5UiTools.prl
%{_qt5_libdir}/cmake/Qt5UiTools/
%{_qt5_libdir}/pkgconfig/Qt5UiTools.pc
%{_qt5_archdatadir}/mkspecs/modules/qt_lib_uitools.pri
%{_qt5_archdatadir}/mkspecs/modules/qt_lib_uitools_private.pri

%if ! 0%{?no_examples:1}
%files examples
%{_qt5_examplesdir}/
%{_qt5_plugindir}/designer/*
%dir %{_qt5_libdir}/cmake/Qt5Designer
%{_qt5_libdir}/cmake/Qt5Designer/Qt5Designer_*
%endif


%changelog
* Thu Aug 29 2024 Funda Wang <fundawang@yeah.net> - 5.15.10-4
- Disable automatic .la file removal
- cleanup spec

* Wed Jan 03 2024 wangqia <wangqia@uniontech.com> - 5.15.10-3
- rebuild for clang update

* Wed Aug 30 2023 douyan <douyan@kylinos.cn> - 5.15.10-2
- add 0001-update-Qt5LinguistToolsMacros.cmake.patch

* Tue Aug 22 2023 douyan <douyan@kylinos.cn> - 5.15.10-1
- update to upstream version 5.15.10

* Wed Mar 30 2022 ouyangminxiang <ouyangminxiang@kylinsec.com.cn> - 5.15.2-4
- Add Chinese translation

* Fri Mar 11 2022 pei-jiankang <peijiankang@kylinos.cn> - 5.15.2-3
- modify lupdate-qt5 run error

* Tue Feb 22 2022 pei-jiankang <peijiankang@kylinos.cn> - 5.15.2-2
- modify lupdate-qt5 run error

* Wed Oct 13 2021 pei-jiankang <peijiankang@kylinos.cn> - 5.15.2-1
- update to upstream version 5.15.2

* Mon Sep 14 2020 liuweibo <liuweibo10@huawei.com> - 5.11.1-5
- Fix Source0 

* Fri Jan 10 2020 zhujunhao <zhujunhao5@huawei.com> - 5.11.1-4
- change the url to valid address

* Wed Nov 27 2019 Shuaishuai Song <songshuaishuai2@huawei.com> - 5.11.1-3
- package init
